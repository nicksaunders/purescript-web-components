module Web.Component.CustomElement (AttributeChangedCallback, CustomElement, CustomElementSpec, customElement) where

import Prelude
import Data.Maybe (Maybe)
import Data.Nullable (Nullable, toMaybe)
import Effect (Effect)
import Web.DOM.Element (Element)

type AttributeChangedCallback attr = String -> attr -> attr -> Element -> Effect Unit

type CustomElementSpec attr =
  { init :: Element -> Effect Unit
  , observedAttributes :: Array String
  , attributeChangedCallback :: AttributeChangedCallback attr
  , connectedCallback :: Element -> Effect Unit
  , disconnectedCallback :: Element -> Effect Unit
  }

foreign import data CustomElement :: Type

customElement :: CustomElementSpec (Maybe String) -> CustomElement
customElement { init, observedAttributes, attributeChangedCallback, connectedCallback, disconnectedCallback } =
  _customElement
    { init
    , observedAttributes
    , attributeChangedCallback: wrapAttributeChangedCallback attributeChangedCallback
    , connectedCallback
    , disconnectedCallback
    }
  where

    wrapAttributeChangedCallback
      :: forall a b c d e
       . (a -> Maybe b -> Maybe c -> d -> e)
      -> (a -> Nullable b -> Nullable c -> d -> e)
    wrapAttributeChangedCallback callback =
      \attribute oldValue newValue el ->
        callback attribute (toMaybe oldValue) (toMaybe newValue) el

foreign import _customElement :: CustomElementSpec (Nullable String) -> CustomElement
