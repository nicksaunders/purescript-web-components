"use strict";

exports._attachShadow = function (init) {
  return function (element) {
    return function () {
      return element.attachShadow(init);
    };
  };
};

exports.shadowRoot = function (element) {
  return function () {
    return element.shadowRoot;
  };
};
