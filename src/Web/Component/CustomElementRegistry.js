"use strict";

exports.define = function (name) {
  return function (customElement) {
    return function (registry) {
      return function () {
        registry.define(name, customElement);
      };
    };
  };
};
