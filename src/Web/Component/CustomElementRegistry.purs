module Web.Component.CustomElementRegistry (CustomElementRegistry, define) where

import Prelude
import Effect (Effect)
import Web.Component.CustomElement (CustomElement)

foreign import data CustomElementRegistry :: Type

foreign import define :: String -> CustomElement -> CustomElementRegistry -> Effect Unit
